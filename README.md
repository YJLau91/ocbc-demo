OCBC CLI Program
==================

This code simulate interaction with a retail bank with some basic commands. 

How to use
=========
Windows
1. Open [Command Prompt] by searching "cmd" in windows.  
2. Navigate to the .jar file that attached in the folder. 
3. Run the jar file by following command ~\demo_jar> java -jar demo.jar

Mac
1. Open a command prompt with CTRL + ALT + T.
2. Go to the demo ".jar" file directory. If your Ubuntu version / flavour supports it, you should be able to right click on your ".jar" file's directory and click "Open in Terminal"
Type the following command: java -jar jarfilename. jar.

Assumption
=========
1. There is no top up limit for this simulation. 
2. No Database involve in this simulation, all data will be reset once program terminated.
3. Client allow to transfer any amount to others client. (regardless the debt/ owning) 
4. Available clients in test data. 
   
            {
                "username": "Jarry",
                "amount": "1000.00",
                "is_login": "1",
                "created_at": "2020/01/04 9:00",
                "last_login": "2020/01/04 9:00"
            },
            {
                 "username": "Bob",
                 "amount": "500.00",
                 "is_login": "0",
                 "created_at": "2019/12/03 11:00",
                 "last_login": "2019/12/03 11:00"
            },
            {
                 "username": "Alice",
                 "amount": "0.00",
                 "is_login": "0",
                 "created_at": "2020/03/10 8:00",
                 "last_login": "2020/03/10 8:00"
            }
            


Available command
=========

**login `client`**                    : Login as `client`. Creates a new client if not yet exists.                                   
**topup `amount`**                    : Increase logged-in client balance by `amount`.                                               
**history**                           : Check client's activity log,                                                                 
**balance**                           : Check client's balance.                                                                      
**pay `[another_client]` `<amount>`** : Pay `amount` from logged-in client to `another_client`, maybe in parts, as soon as possible. 



Contact
=========
Developed by Lau Ying Jian<br />
yingjianlau@hotmail.com<br />


