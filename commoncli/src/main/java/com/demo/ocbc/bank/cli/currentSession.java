package com.demo.ocbc.bank.cli;

import java.util.List;

public class currentSession {
    public static List<UserModel> UserList;
    public static List<UserTransactionLogModel> UserTransactionLogList;
    public static List<OwnListModel> UserOwnList;

    public static List<UserModel> getUserList() {
        return UserList;
    }

    public static List<UserTransactionLogModel> getUserTransactionLogList() {
        return UserTransactionLogList;
    }

    public static void setUserList(List<UserModel> userList) {
        UserList = userList;
    }

    public static void setUserTransactionLogList(List<UserTransactionLogModel> userTransactionLogList) {
        UserTransactionLogList = userTransactionLogList;
    }

    public static List<OwnListModel> getUserOwnList() {
        return UserOwnList;
    }

    public static void setUserOwnList(List<OwnListModel> userOwnList) {
        UserOwnList = userOwnList;
    }
}
