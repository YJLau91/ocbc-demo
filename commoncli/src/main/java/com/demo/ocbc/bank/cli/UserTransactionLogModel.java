package com.demo.ocbc.bank.cli;

public class UserTransactionLogModel {
    public  String account_name;
    public  String description;
    public  String created_at;

    public void setAccount_name (String account_name) {
        this.account_name = account_name;
    }
    public String getAccount_name() {
        return this.account_name;
    }

    public void setDescription (String description) {
        this.description = description;
    }
    public String getDescription() {
        return this.description;
    }

    public void setCreated_at (String created_at) {
        this.created_at = created_at;
    }
    public String getCreated_at() {
        return this.created_at;
    }

}
