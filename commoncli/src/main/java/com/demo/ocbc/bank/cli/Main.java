package com.demo.ocbc.bank.cli;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        currentSession.setUserList(loadUser());
        currentSession.setUserTransactionLogList(loadUserTransactionLog());
        currentSession.setUserOwnList(loadUserOwnList());


        System.out.println("|----------------------------------------------------------------------------------------------------------------------------------|");
        System.out.println("|                                          Welcome to Bank Command Line Interface                                                  |");
        System.out.println("|----------------------------------------------------------------------------------------------------------------------------------|");

        System.out.println("|----------------------------------------------------------------------------------------------------------------------------------|");
        System.out.println("| Command                           | Description                                                                                  |");
        System.out.println("| --------------------------------- | -------------------------------------------------------------------------------------------- |");
        System.out.println("| login `client`                    | Login as `client`. Creates a new client if not yet exists.                                   |");
        System.out.println("| topup `amount`                    | Increase logged-in client balance by `amount`.                                               |");
        System.out.println("| history                           | Check client's activity log,                                                                 |");
        System.out.println("| balance                           | Check client's balance.                                                                      |");
        System.out.println("| pay `[another_client]` `<amount>` | Pay `amount` from logged-in client to `another_client`, maybe in parts, as soon as possible. |");
        System.out.println("|----------------------------------------------------------------------------------------------------------------------------------|");

        String input;
        do{
            input = sc.nextLine();
            OperationUtils.getOperation(input);
            //displayCurrent(currentSession.getUserList());

        } while(!input.equals("exit"));
        sc.close();

	}

	public static List<UserModel> loadUser()
    {
        List<UserModel> UserList = new ArrayList<UserModel>();

        ResourceUtils ru = new ResourceUtils();
        String json = null;
        try {
            json = ru.getString("/userlist.txt");
        } catch (IOException e) {
            e.printStackTrace();
        }
        JSONObject obj = new JSONObject(json);

        JSONArray arr = obj.getJSONArray("UserList");
        for (int i = 0; i < arr.length(); i++) {

            UserModel currentUser = new UserModel();
            String username = arr.getJSONObject(i).getString("username");
            String amount = arr.getJSONObject(i).getString("amount");
            String last_login = arr.getJSONObject(i).getString("last_login");

            currentUser.setAccount_name(username);
            currentUser.setBalance(Double.parseDouble(amount));
            currentUser.setIs_login(false);
            currentUser.setLast_login(last_login);

            UserList.add(currentUser);
        }

        return UserList;

    }

    public static List<UserTransactionLogModel> loadUserTransactionLog()
    {
        List<UserTransactionLogModel> UserTransactionList = new ArrayList<UserTransactionLogModel>();

        ResourceUtils ru = new ResourceUtils();
        String json = null;
        try {
            json = ru.getString("/user_transaction_log.txt");
        } catch (IOException e) {
            e.printStackTrace();
        }
        JSONObject obj = new JSONObject(json);

        JSONArray arr = obj.getJSONArray("trnList");
        for (int i = 0; i < arr.length(); i++) {

            UserTransactionLogModel currentUserLog = new UserTransactionLogModel();
            String username = arr.getJSONObject(i).getString("username");
            String description = arr.getJSONObject(i).getString("description");
            String created_at = arr.getJSONObject(i).getString("created_at");

            currentUserLog.setAccount_name(username);
            currentUserLog.setDescription(description);
            currentUserLog.setCreated_at(created_at);

            UserTransactionList.add(currentUserLog);
        }

        return UserTransactionList;

    }

    public static List<OwnListModel> loadUserOwnList()
    {
        List<OwnListModel> UserOwnList = new ArrayList<OwnListModel>();

        ResourceUtils ru = new ResourceUtils();
        String json = null;
        try {
            json = ru.getString("/ownList.txt");
        } catch (IOException e) {
            e.printStackTrace();
        }
        JSONObject obj = new JSONObject(json);

        JSONArray arr = obj.getJSONArray("OwnList");
        for (int i = 0; i < arr.length(); i++) {

            OwnListModel currentList = new OwnListModel();
            String username = arr.getJSONObject(i).getString("username");
            Double amount = arr.getJSONObject(i).getDouble("amount");
            String Payto = arr.getJSONObject(i).getString("Payto");

            currentList.setAccount_name(username);
            currentList.setAmount(amount);
            currentList.setPayTo(Payto);

            UserOwnList.add(currentList);
        }

        return UserOwnList;

    }

}
