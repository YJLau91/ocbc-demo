package com.demo.ocbc.bank.cli;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class OperationUtils {

    public static void getOperation(String command) {


        if (command.startsWith("login")) {
            User_Login(command);
            displayCurrent(currentSession.getUserList());
        } else if (command.startsWith("topup")) {
            User_TopUp(command);
        } else if (command.startsWith("pay")) {
            User_Transfer(command);
            displayCurrent(currentSession.getUserList());
        } else if (command.startsWith("help")) {
            get_Help();
        } else if (command.startsWith("history")) {
            get_log();
        } else if (command.startsWith("balance")) {
            displayCurrent(currentSession.getUserList());
        } else if (command.startsWith("exit")) {
            System.out.println("Bye! Hope to see you soon!");
        } else {
            System.out.println("Command not found. Those are the available commands. ");
            get_Help();
        }
    }

    public static void User_Login(String command) {

        boolean is_user_exist = false;

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();

        String account_name = command.replace("login ", "");
        for (UserModel user : currentSession.getUserList()) {
            if (user.getAccount_name().equals(account_name)) {
                // display log in info
                System.out.println("Hi, Welcome back! " + user.getAccount_name());
                System.out.println("Your current balance is: RM" + user.getBalance());
                System.out.println("Your last log in: " + user.getLast_login());

                //set is login to true, update last login date time
                user.setIs_login(true);
                user.setLast_login(dateFormat.format(date));
                is_user_exist = true;

                // add log
                UserTransactionLogModel log = new UserTransactionLogModel();
                log.setAccount_name(user.getAccount_name());
                log.setDescription("log in. ");
                log.setCreated_at(dateFormat.format(date));

                currentSession.getUserTransactionLogList().add(log);

            } else { //log out others users
                user.setIs_login(false);
            }
        }

        // create user if not exist
        if (!is_user_exist) {
            UserModel new_user = new UserModel();
            new_user.setAccount_name(account_name);
            new_user.setBalance(0);
            new_user.setIs_login(true);
            new_user.setLast_login(dateFormat.format(date));

            currentSession.getUserList().add(new_user);

            // add log
            UserTransactionLogModel log = new UserTransactionLogModel();
            log.setAccount_name(new_user.getAccount_name());
            log.setDescription("Account created. ");
            log.setCreated_at(dateFormat.format(date));

            currentSession.getUserTransactionLogList().add(log);

            System.out.println("User not found. New user created.");
        }

    }

    public static void User_TopUp(String command) {

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();

        double topup_amount = Double.parseDouble(command.replace("topup ", ""));
        for (UserModel user : currentSession.getUserList()) {
            if (user.getIs_login()) {
                double currentAmount = user.getBalance();
                user.setBalance(currentAmount + topup_amount);

                // add log
                UserTransactionLogModel log = new UserTransactionLogModel();
                log.setAccount_name(user.getAccount_name());
                log.setDescription("top up RM" + topup_amount);
                log.setCreated_at(dateFormat.format(date));

                currentSession.getUserTransactionLogList().add(log);
            }
        }
        System.out.println("Your have top up RM" + topup_amount + "!");
    }

    public static void User_Transfer(String command) {

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        String senderName = "";

        List<String> splitCommand = getTransferDetails(command.replace("pay ", ""));

        if (splitCommand.size() == 0) {
            System.out.println("Invalid command. Please follow the correct format : pay `[another_client]` `<amount>`");
            return;
        }

        Double transferAmount = Double.valueOf(splitCommand.get(1));
        String recipient = splitCommand.get(0);
        boolean is_recipient_found = false;

        for (UserModel user : currentSession.getUserList()) {
            if (user.getIs_login()) {
                if (user.getBalance() < transferAmount) {
                    System.out.println("You have insufficient fund to perform the payment. ");
                } else {
                    user.setBalance(user.getBalance() - transferAmount);
                    senderName = user.getAccount_name();
                }
            }
        }
        for (UserModel user : currentSession.getUserList()) {
            if (user.getAccount_name().equals(recipient)) {
                user.setBalance(user.getBalance() + transferAmount);
                is_recipient_found = true;

                // add log
                UserTransactionLogModel receive_log = new UserTransactionLogModel();
                receive_log.setAccount_name(user.getAccount_name());
                receive_log.setDescription("Receive RM" + transferAmount + " from " + senderName);
                receive_log.setCreated_at(dateFormat.format(date));

                currentSession.getUserTransactionLogList().add(receive_log);

                UserTransactionLogModel pay_log = new UserTransactionLogModel();
                pay_log.setAccount_name(senderName);
                pay_log.setDescription("Pay RM" + transferAmount + " to " + user.getAccount_name());
                pay_log.setCreated_at(dateFormat.format(date));

                currentSession.getUserTransactionLogList().add(pay_log);
            }
        }

        //perform refund
        if (!is_recipient_found) {
            for (UserModel user : currentSession.getUserList()) {
                if (user.getIs_login()) {
                    user.setBalance(user.getBalance() + transferAmount);
                }
            }
            System.out.println("Recipient not found. ");
        }else
        {
            for (OwnListModel list: currentSession.getUserOwnList())
            {
                if (list.getAccount_name().equals(senderName) && list.getAmount() > 0 &&list.getPayTo().equals(recipient)) {
                    list.setAmount(list.getAmount() - transferAmount);
                }
            }

            System.out.println("Payment success! ");
        }

    }

    public static void get_Help() {
        System.out.println("|----------------------------------------------------------------------------------------------------------------------------------|");
        System.out.println("| Command                           | Description                                                                                  |");
        System.out.println("| --------------------------------- | -------------------------------------------------------------------------------------------- |");
        System.out.println("| login `client`                    | Login as `client`. Creates a new client if not yet exists.                                   |");
        System.out.println("| topup `amount`                    | Increase logged-in client balance by `amount`.                                               |");
        System.out.println("| history                           | Check client's activity log,                                                                 |");
        System.out.println("| balance                           | Check client's balance.                                                                      |");
        System.out.println("| pay `[another_client]` `<amount>` | Pay `amount` from logged-in client to `another_client`, maybe in parts, as soon as possible. |");
        System.out.println("|----------------------------------------------------------------------------------------------------------------------------------|");
    }

    public static void get_log() {

        List<UserTransactionLogModel> trnLogs = currentSession.getUserTransactionLogList();
        String activeUser = "";
        for (UserModel user : currentSession.getUserList()) {

            if (user.getIs_login()) {
                activeUser = user.getAccount_name();
            }
        }

        if (activeUser == "") {
            System.out.println("Please log in first. ");
            return;
        }


        for (UserTransactionLogModel log : trnLogs) {
            if (log.getAccount_name() == activeUser) {
                System.out.println(" - " +log.getCreated_at() + ": " + log.getDescription());
            }
        }


    }

    public static ArrayList<String> getTransferDetails(String command) {
        //initialized the counter to 0
        int startIndex = -1;
        int endIndex = -1;

        ArrayList<String> result = new ArrayList<String>();


        for (int i = 0; i <= command.length() - 1; i++) {
            if (command.charAt(i) == '[') {
                //increasing the counter value at each occurrence of 'B'
                startIndex = i;
            }
        }

        for (int i = 0; i <= command.length() - 1; i++) {
            if (command.charAt(i) == ']') {
                //increasing the counter value at each occurrence of 'B'
                endIndex = i;
            }
        }

        if (startIndex == -1 || endIndex == -1) {
            return result;
        }

        String recipient = command.substring(startIndex + 1, endIndex);
        String amount = command.substring(endIndex + 1);

        result.add(recipient);
        result.add(amount);

        return result;
    }

    public static void displayCurrent(List<UserModel> userList) {

        String activeUser = "";

        for (UserModel user : userList) {
            if (user.getIs_login()) {
                System.out.println("Current User   : " + user.getAccount_name());
                System.out.println("Current Balance: RM" + user.getBalance());
                activeUser = user.getAccount_name();
            }
        }

        for (OwnListModel list: currentSession.getUserOwnList())
        {
            if (list.getAccount_name().equals(activeUser) && list.getAmount() > 0) {
                System.out.println("Owning   : " + list.getPayTo() + " RM" + list.getAmount());
            }
        }

        if (activeUser == "") {
            System.out.println("Please log in first. ");
            return;
        }
    }

}
