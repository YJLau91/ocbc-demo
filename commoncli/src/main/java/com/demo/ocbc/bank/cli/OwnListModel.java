package com.demo.ocbc.bank.cli;

public class OwnListModel {
    public  String account_name;
    public  double amount;
    public  String PayTo;

    public void setAccount_name (String account_name) {
        this.account_name = account_name;
    }
    public String getAccount_name() {
        return this.account_name;
    }

    public void setAmount (double amount) {
        this.amount = amount;
    }
    public double getAmount() {
        return this.amount;
    }

    public void setPayTo (String PayTo) {
        this.PayTo = PayTo;
    }
    public String getPayTo() {
        return this.PayTo;
    }

}
