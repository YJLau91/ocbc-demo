package com.demo.ocbc.bank.cli;

public class UserModel {
    public String account_name;
    public boolean is_login;
    public double balance;
    public String last_login;

    public void setAccount_name (String account_name) {
        this.account_name = account_name;
    }
    public String getAccount_name() {
        return this.account_name;
    }

    public void setIs_login (boolean is_login) {
        this.is_login = is_login;
    }
    public boolean getIs_login() {
        return this.is_login;
    }

    public void setBalance (double balance) {
        this.balance = balance;
    }
    public double getBalance() {
        return this.balance;
    }

    public void setLast_login (String last_login) {
        this.last_login = last_login;
    }
    public String getLast_login() {
        return this.last_login;
    }

}
